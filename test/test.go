package main

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"github.com/goburrow/modbus"
	"math"
	"time"
)

func main() {
	hander := modbus.NewRTUClientHandler("/dev/ttyUSB0")
	hander.BaudRate = 9600
	hander.DataBits = 8
	hander.Parity = "N"
	hander.StopBits = 1
	hander.SlaveId = 1
	hander.Timeout = 5 * time.Second
	//hander.Logger = log.New(os.Stdout, "test: ", log.LstdFlags)

	err := hander.Connect()
	defer func() {
		_ = hander.Close()
	}()

	if err != nil {
		fmt.Println(err)
		return
	}

	client := modbus.NewClient(hander)
	ports := []uint16{0, 0x46}
	for _, i := range ports {
		results, err := client.ReadInputRegisters(i, 2)
		if err != nil {
			fmt.Printf("%d %s\n", i, err)
			continue
		}
		if binary.LittleEndian.Uint16(results) == 0 {
			continue
		}
		v := Float64frombytes(results)
		fmt.Printf("%d\t%d\t%f\t\t%s", i, binary.LittleEndian.Uint16(results), v, hex.Dump(results))
	}
	/*for _, i := range ports {
		results, err := client.ReadInputRegisters(i, 2)
		if err != nil {
			fmt.Printf("%d %s\n", i, err)
			continue
		}
		if binary.LittleEndian.Uint16(results) == 0 {
			continue
		}
		fmt.Printf("%d\t%d\t%d\t\t%s", i, binary.LittleEndian.Uint16(results), binary.BigEndian.Uint16(results), hex.Dump(results))
	}*/
}

func Float64frombytes(bytes []byte) float32 {
	bits := binary.BigEndian.Uint32(bytes)
	float := math.Float32frombits(bits)
	return float
}
